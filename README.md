# Dasha_Kozmodemianova_Portfolio



## Project description

Project "TaskChecker" - project to create tests and collect answers to tests

## Requirements that the autotest checks

1. Go to http://checker.jettycloud.com/task/{uid}.
Where uid is an identifier that exists in the database and has not yet been validated.
The test page is open.
The participant's first and last name correctly displays in the page, that is, they correspond to the specified uid.
There is an inscription "Time left" on the page, time decreases.
The page shows the number of questions. There are 34 questions. The first question is highlighted in green, the rest are grey.
There is a question with multiple answers on the page.
There is a "Следующий вопрос" button on the page. The button is not active, cannot be clicked, clicking on it does not change anything.
2. Choose one of the answer options, click on it. The "Следующий вопрос" button is enabled.
3. Click on the "Следующий вопрос" button. Moved to the next question. In the list of questions, the popup question shows a checkmark, the question has turned green. "Следующий вопрос" button is disabled.
4. Repeat option 2-3 for all until the last question (33 times). Choice of answer on a random basis. For data entry questions, prepare any correct data:
Number / SQL code / Java code. In the last question the button should change the name to "Завершить тест".
5. Click on the "Завершить тест" button. There was a transition to the page: http://checker.jettycloud.com/complete. The page has an image with a check mark and text: "Задание успешно отправлено!
   Мы дадим обратную связь до 13-го марта 2022г. Если обратная связь тебе не пришла, или у тебя возникли вопросы, пиши нам на почту". There is also an email button below the text: "bootcamp@dins.ru"

## Test result

Unfortunately, it will not work to check the operation of this test, because I have attached only the part that I personally wrote. But it  requires an additional piece of code to work, for which I do not have rights. Also, after completing the training, I do not have access to resources. Autotest checks all requirements. All tests pass with "Passed". My training mentor made comments to me on the merge request, which I did not have time to correct before the end of the course. I put this code to show that I can use automated testing.
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.logging.Logger;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

public class TestWebСhecker extends Common {
    String uid = "a97586e8-81c4-11ed-a1eb-0242ac120002";
    private static final String COLOR_GREEN = "rgba(76, 175, 80, 1)";
    private static final String COLOR_WHITE = "rgba(255, 255, 255, 1)";

    private final Logger logger = Logger.getLogger(FirstTest.class.getName());

    @Test
    public void bigTest() throws Exception {
        //Проверка стартовой страницы

        webDriver.get(baseUrl.concat("/task/" + uid)); //Открылась страница теста http://checker.jettycloud.com/task/uid
        Thread.sleep(1000);

        checkName(); //На странице корректно отображено имя и фамилия участника, то есть соответствуют заданному uid
        checkInscriptionTimeLeft(); //На странице есть надписать Time left, время уменьшается
        checkSetQuestions(); //На странице показано кол-во вопросов. Вопросов 34.
        checkColorQuestions(); // Первый вопрос выделен зеленым цветом, остальные серые.
        checkQuestionAndAnswers(); //На странице есть вопрос с вариантами ответов.
        checkButtonNextQuestionNotActive(); //На странице есть кнопка “Следующий вопрос”. Кнопка не активна, не может быть нажата, клики по ней ничего не изменяют
        for (int numberQuestion = 1; numberQuestion < 26; numberQuestion++) {//Повторите пункты 2-3 для всех до последнего вопроса (33 раза). Вариант ответа выбраны рандомно.Для вопросов с вводом данных подготовьте любые корректные данные:
            //Число, Sql code, java code.
            choiceColorAnswer(numberQuestion); //В списке вопросов предыдущий вопрос показывает галочку, текущий вопрос стал зеленым.
            choiceAnswer(numberQuestion);//На странице есть кнопка “Следующий вопрос”. Кнопка не активна, не может быть нажата, клики по ней ничего не изменяют.
            // Выберите один из вариантов ответа, нажмите на него. Кнопка “Следующий вопрос” стала активной. Нажмите на кнопку “Следующий вопрос”.
            //Произошел переход на следующий вопрос. В последнем вопросе кнопка должна изменить имя на “Завершить тест”
        }
        //Произошел переход на страницу http://checker.jettycloud.com/complete. На странице есть картинка с галочкой и текстом: “Задание успешно отправлено!
        //Мы дадим обратную связь до 13-го марта 2022г. Если обратная связь тебе не пришла, или у тебя возникли вопросы, пиши нам на почту.”
        //Также под текстом есть кнопка с имейлом: “bootcamp@dins.ru”
        checkLastPage();
    }

    public void checkName() throws Exception {
        String name = webDriver.findElement(By.className("ant-typography")).getText();
        logger.info(name);
        assertEquals(name, "Аня Петрова");
    }

    public void checkInscriptionTimeLeft() throws Exception {
        String timeFirst = webDriver.findElement(By.id("timer")).getText();
        String secondFirst = timeFirst.substring(14);
        int intSecondFirst = Integer.parseInt(secondFirst);
        String minuteFirst = timeFirst.substring(11, 13);
        int intMinuteFirst = Integer.parseInt(minuteFirst);
        logger.info(timeFirst);

        Thread.sleep(1000);

        String timeSecond = webDriver.findElement(By.id("timer")).getText();
        String secondSecond = timeSecond.substring(14);
        int intSecondSecond = Integer.parseInt(secondSecond);
        String minuteSecond = timeSecond.substring(11, 13);
        int intMinuteSecond = Integer.parseInt(minuteSecond);
        logger.info(timeSecond);

        if (intSecondFirst != 0) {
            assertTrue(intSecondFirst > intSecondSecond);
        } else {
            assertTrue(intMinuteFirst > intMinuteSecond);
        }
    }

    public void checkSetQuestions() throws Exception {
        String numberQuestions = webDriver.findElement(By.className("ant-steps")).getText();
        logger.info(numberQuestions);
        String[] array = numberQuestions.split("\n");
        int SetQuestions = array.length;
        assertEquals(SetQuestions, 25);
    }

    public void checkColorQuestions() throws Exception {
        checkColorQuestion(1, COLOR_GREEN);
        for (int numberQuestion = 2; numberQuestion < 26; numberQuestion++) {
            checkColorQuestion(numberQuestion, COLOR_WHITE);
        }
    }

    public void checkColorQuestion(int numberQuestion, String colorQuestion) {
        String numberColorQuestion1 = webDriver.findElement(By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[1]/div[2]/div[" + numberQuestion + "]/div/div[2]")).getCssValue("background-color");
        logger.info(numberColorQuestion1);
        assertEquals(numberColorQuestion1, colorQuestion);
    }

    public void checkQuestionAndAnswers() throws Exception {
        String question = webDriver.findElement(By.className("ant-card-head-title")).getText();
        logger.info(question);

        for (int numberAnswer = 1; numberAnswer < 5; numberAnswer++) {
            String answer = webDriver.findElement(
                    By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div/div/div/div[" + numberAnswer + "]")).getText();
            logger.info(answer);
        }
    }

    public void checkButtonNextQuestionNotActive() throws Exception {
        WebElement buttonNextQuestion = webDriver.findElement(By.className("ant-btn"));
        buttonNextQuestion.click(); // Не понятно зачем тут нажимать на кнопку
        assertFalse(buttonNextQuestion.isEnabled());
    }

    public void checkButtonNextQuestionActiveOrNotActive(String buttonName) {
        WebElement buttonNextQuestion = webDriver.findElement(By.className("ant-btn"));
        assertTrue(buttonNextQuestion.isEnabled());

        String nameButtonNextQuestion = buttonNextQuestion.getText();
        assertEquals(nameButtonNextQuestion, buttonName);

        buttonNextQuestion.click();
    }

    public void choiceAnswer(int numberQuestion) throws Exception {
        if (numberQuestion == 19) {
            WebElement AnswerFromNumbers = webDriver.findElement(By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div/div/div[2]/div/div/input"));
            Thread.sleep(3000);
            AnswerFromNumbers.sendKeys("12");
            checkButtonNextQuestionActiveOrNotActive("Следующий вопрос");
            return;
        }
        if (numberQuestion == 21) {
            int index = 1 + (int) (Math.random() * 3);
            WebElement answer = webDriver.findElement(By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div/div/div/div[" + index + "]/label/span[1]/input"));
            answer.click();
            Thread.sleep(1000);
            checkButtonNextQuestionActiveOrNotActive("Следующий вопрос");
            return;
        }
        if (numberQuestion == 22 || numberQuestion == 23) {
            WebElement answer = webDriver.findElement(By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div[2]/div/div/textarea"));
            Thread.sleep(3000);
            answer.sendKeys("select * from Customers\n" +
                    "WHERE City = 'London'");
            Thread.sleep(1000);
            checkButtonNextQuestionActiveOrNotActive("Следующий вопрос");
            return;
        }
        if (numberQuestion == 24 || numberQuestion == 25) {
            WebElement answer = webDriver.findElement(By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div/div/div/textarea"));
            Thread.sleep(3000);
            answer.sendKeys("class App {\n" +
                    "    public static void main(String[] args) {\n" +
                    "        System.out.println(\"Hello, World!\");\n" +
                    "    }\n" +
                    "}");
            Thread.sleep(1000);

            if (numberQuestion == 24) {
                checkButtonNextQuestionActiveOrNotActive("Следующий вопрос");
            } else {
                checkButtonNextQuestionActiveOrNotActive("Завершить тест");
                String url = webDriver.getCurrentUrl(); //2 строчки выделить метод checkUrl
                assertEquals(url, "http://checker.jettycloud.com/complete");
            }
            return;
        }

        int index = 1 + (int) (Math.random() * 4);
        WebElement answer = webDriver.findElement(
                By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div/div/div/div[" + index + "]/label/span[1]/input")
        );
        answer.click();
        Thread.sleep(1000);
        checkButtonNextQuestionActiveOrNotActive("Следующий вопрос");
    }

    public void checkLastPage() throws Exception {
        webDriver.findElement(By.cssSelector(".ant-result-success .ant-result-icon>.anticon"));

        String checkText1 = webDriver.findElement(By.className("ant-result-title")).getText();
        assertEquals(checkText1, "Задание успешно отправлено!");

        String checkText2 = webDriver.findElement(By.className("ant-result-subtitle")).getText();
        assertEquals(checkText2, "Мы дадим обратную связь до 13-го марта 2022г. Если обратная связь тебе не пришла, или у тебя возникли вопросы, пиши нам на почту.");

        WebElement buttonEmail = webDriver.findElement(By.className("ant-typography"));
        assertTrue(buttonEmail.isEnabled());

        String buttonEmailText = webDriver.findElement(By.className("ant-typography")).getText();
        assertEquals(buttonEmailText, "bootcamp@dins.ru");
    }

    public void choiceColorAnswer(int numberQuestion) throws Exception {
        if (numberQuestion == 1) {
            return;
        }
        Thread.sleep(1000);
        String previousQuestion = webDriver.findElement(By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[1]/div[2]/div[" + (numberQuestion - 1) + "]/div/div[2]/span/span")).getAttribute("role");
        logger.info(previousQuestion);
        assertEquals(previousQuestion, "img");

        String currentQuestion = webDriver.findElement(By.xpath("//*[@id=\"root\"]/section/main/div[2]/div/div/div[1]/div[2]/div[" + numberQuestion + "]/div/div[2]")).getCssValue("background-color");
        logger.info(currentQuestion);
        assertEquals(currentQuestion, COLOR_GREEN);
    }
}